Makefile for compiling to Teensy++, this uses the teensy_loader_cli.
After running make program, a manual hit of the reset button on the teensy is required.
This manual reset can be removed later by toggling the reset pin on the teensy with a gpio pin.
