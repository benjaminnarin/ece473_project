/*
 ** author: Benjamin Narin
 ** date: 10/20/15
 ** demo_code for testing lcd drivers and twi on atmega128
 */

#include <stdio.h>
#include "hw_atmega128.h"
#include "io_spi.h"
#include "hd44780_driver/hd44780.h"
#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "lm73_functions.h"

#define DEBUG_TWI 1 

char greeting[] ="Hello";
char lcd_string_array[16];                    //holds string to send to lcd

extern uint8_t lm73_wr_buf[2];
extern uint8_t lm73_rd_buf[2];

int init_mcu(void){
	spi_init();      //initalize SPI 
	lcd_init();      //initalize LCD 
	init_twi();
	clear_display(); //manually clear LCD display 
	cursor_off();    //keep LCD cursor off
	uint16_t lm73_temp;

	#if DEBUG_TWI
	//set LM73 mode for reading temperature by loading pointer register
	//this is done outside of the normal interrupt mode of operation 
	lm73_wr_buf[0] = LM73_PTR_TEMP;		//load lm73_wr_buf[0] with temperature pointer address
	twi_start_wr(LM73_ADDRESS, lm73_wr_buf, 1);				//start the TWI write process (twi_start_wr())
	sei();					//enable interrupts to allow start_wr to finish
	#endif

	while(1){   //main while loop to make LCD visible
		string2lcd(greeting);
		cursor_home();
		 _delay_ms(2);

		#if DEBUG_TWI
		home_line2();
		// _delay_ms(100);  //tenth second wait
		// clear_display(); //wipe the display
		twi_start_rd(LM73_ADDRESS, lm73_rd_buf, 2); //read temperature data from LM73 (2 bytes)  (twi_start_rd())
		_delay_ms(2);    //wait for it to finish
		//now assemble the two bytes read back into one 16-bit value
		lm73_temp = (lm73_rd_buf[1] << 8);		//save high temperature byte into lm73_temp
		//................ //shift it into upper byte 
		lm73_temp |= lm73_rd_buf[0];			//"OR" in the low temp byte to lm73_temp 
		itoa(lm73_temp, lcd_string_array, 10);	//convert to string in array with itoa() from avr-libc                           
		string2lcd(lcd_string_array);			//send the string to LCD (lcd_functions)
		_delay_ms(100);  //tenth second wait
		clear_display();
		#endif
	} //while
} //main
