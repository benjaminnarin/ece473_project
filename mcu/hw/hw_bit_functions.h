/*
 * HW_BIT_FUNCTIONS.h
 *
 * Created: 11/6/15 21:85pm
 *  Author: rkalb
 */ 


#ifndef HW_BIT_FUNCTIONS_H
#define HW_BIT_FUNCTIONS_H

/******************************************************************************
 *                              I N C L U D E S                               *
 ******************************************************************************/

#include <avr/io.h>

/******************************************************************************
 *                               D E F I N E S                                *
 ******************************************************************************/

#define bit_set(...)    bit_set_(__VA_ARGS__)
#define bit_set_(x,y)   x |= 1<<y     // set a bit
#define bit_clear(...)    bit_clear_(__VA_ARGS__)
#define bit_clear_(x,y)   x &= ~(1<<y)      // clear a bit
#define bit_test(...)   bit_test_(__VA_ARGS__)
#define bit_test_(x,y)    (!!(x & (1<<y)))    // test a bit

/*** following macros related to the PORTx - register only !  ***/
/***                ***/
#define bit_dir_output(...) bit_dir_outp_(__VA_ARGS__)
#define bit_dir_output_(x,y)  *(&x-1) |= 1<<y     // access DDRx of PORTx !
#define bit_dir_input(...)  bit_dir_inp_(__VA_ARGS__)
#define bit_dir_input_(x,y) *(&x-1) &= ~(1<<y)    // access DDRx of PORTx !
#define bit_test_input(...)  bit_test_in_(__VA_ARGS__)
#define bit_test_input_(x,y) (!!(*(&x-2) & (1<<y)))    // access PINx of PORTx !

/******************************************************************************
 *                              T Y P E D E F S                               *
 ******************************************************************************/


/******************************************************************************
 *          P U B L I C   F U N C T I O N   D E C L A R A T I O N S           *
 ******************************************************************************/

#endif /* HW_BIT_FUNCTIONS_ */
