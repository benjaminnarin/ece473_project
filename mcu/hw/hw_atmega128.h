/*
** author: Ryan Kalb
** date: 10/20/15
*/

#ifndef HW_ATMEGA128_H
#define HW_ATMEGA128_H

#define PROCESSOR "ATMEGA128"

//#include "hw_bit_functions.h"
#include "hw_pin_definitions.h"

/*
** Bit Functionality
*/

#define	bit_set(...)		bit_set_(__VA_ARGS__)
#define	bit_set_(x,y)		x |= 1<<y			// set a bit
#define	bit_clear(...)		bit_clear_(__VA_ARGS__)
#define	bit_clear_(x,y)		x &= ~(1<<y)			// clear a bit
#define	bit_test(...)		bit_test_(__VA_ARGS__)
#define	bit_test_(x,y)		(!!(x & (1<<y)))		// test a bit

/*** following macros related to the PORTx - register only !	***/
/***/

#define	bit_dir_outp(...)	bit_dir_outp_(__VA_ARGS__)
#define	bit_dir_outp_(x,y)	*(&x-1) |= 1<<y			// access DDRxof PORTx !
#define	bit_dir_inp(...)	bit_dir_inp_(__VA_ARGS__)
#define	bit_dir_inp_(x,y)	*(&x-1) &= ~(1<<y)		// access DDRxof PORTx !
#define	bit_test_in(...)	bit_test_in_(__VA_ARGS__)
#define	bit_test_in_(x,y)	(!!(*(&x-2) & (1<<y)))		// access PINxof PORTx !

/*
**   Prototypes
*/

extern int init_mcu();

#endif

