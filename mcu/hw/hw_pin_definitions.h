/*
 * HW_PIN_DEFINITIONS.h
 *
 * Created: 11/6/15 21:85pm
 *  Author: rkalb
 */ 


#ifndef HW_BIT_DEFINITIONS_H
#define HW_BIT_DEFINITIONS_H

/******************************************************************************
 *                              I N C L U D E S                               *
 ******************************************************************************/

#include "hw_bit_functions.h"
#include <avr/io.h>

/******************************************************************************
 *                               D E F I N E S                                *
 ******************************************************************************/

/* Plain Name Declerations */

/* Port A , plain names*/
#define HW_PA0		PORTA, 0
#define HW_PA1		PORTA, 1
#define HW_PA2		PORTA, 2
#define HW_PA3		PORTA, 3
#define HW_PA4		PORTA, 4
#define HW_PA5		PORTA, 5
#define HW_PA6		PORTA, 6
#define HW_PA7		PORTA, 7

/* Port B , plain names*/
#define HW_PB0		PORTB, 0
#define HW_PB1		PORTB, 1
#define HW_PB2		PORTB, 2
#define HW_PB3		PORTB, 3
#define HW_PB4		PORTB, 4
#define HW_PB5		PORTB, 5
#define HW_PB6		PORTB, 6
#define HW_PB7		PORTB, 7

/* Port C , plain names*/
#define HW_PC0		PORTC, 0
#define HW_PC1		PORTC, 1
#define HW_PC2		PORTC, 2
#define HW_PC3		PORTC, 3
#define HW_PC4		PORTC, 4
#define HW_PC5		PORTC, 5
#define HW_PC6		PORTC, 6
#define HW_PC7		PORTC, 7

/* Port D , plain names*/
#define HW_PD0		PORTD, 0
#define HW_PD1		PORTD, 1
#define HW_PD2		PORTD, 2
#define HW_PD3		PORTD, 3
#define HW_PD4		PORTD, 4
#define HW_PD5		PORTD, 5
#define HW_PD6		PORTD, 6
#define HW_PD7		PORTD, 7

/* Port E , plain names*/
#define HW_PE0		PORTE, 0
#define HW_PE1		PORTE, 1
#define HW_PE2		PORTE, 2
#define HW_PE3		PORTE, 3
#define HW_PE4		PORTE, 4
#define HW_PE5		PORTE, 5
#define HW_PE6		PORTE, 6
#define HW_PE7		PORTE, 7

/* Port F , plain names*/
#define HW_PF0		PORTF, 0
#define HW_PF1		PORTF, 1
#define HW_PF2		PORTF, 2
#define HW_PF3		PORTF, 3
#define HW_PF4		PORTF, 4
#define HW_PF5		PORTF, 5
#define HW_PF6		PORTF, 6
#define HW_PF7		PORTF, 7

/* Port G , plain names*/
#define HW_PG0		PORTG, 0
#define HW_PG1		PORTG, 1
#define HW_PG2		PORTG, 2
#define HW_PG3		PORTG, 3
#define HW_PG4		PORTG, 4


/* Functional Name Declerations */

/* Port A, Functional Names */
#define HW_AD0		PORTA, 0
#define HW_AD1		PORTA, 1
#define HW_AD2		PORTA, 2
#define HW_AD3		PORTA, 3
#define HW_AD4		PORTA, 4
#define HW_AD5		PORTA, 5
#define HW_AD6		PORTA, 6
#define HW_AD7		PORTA, 7

/* Port B , Functional Names*/
#define HW_SS		PORTB, 0
#define HW_SCK		PORTB, 1
#define HW_MOSI	PORTB, 2
#define HW_MISO	PORTB, 3
#define HW_OC0		PORTB, 4
#define HW_OC1A	PORTB, 5
#define HW_OC1B	PORTB, 6
#define HW_OC2		PORTB, 7
#define HW_OC1C	PORTB, 7

/* Port C , Functional Names*/
#define HW_A8		PORTC, 0
#define HW_A9		PORTC, 1
#define HW_A10		PORTC, 2
#define HW_A11		PORTC, 3
#define HW_A12		PORTC, 4
#define HW_A13		PORTC, 5
#define HW_A14		PORTC, 6
#define HW_A15		PORTC, 7

/* Port D , Functional Names*/
#define HW_SCL		PORTD, 0
#define HW_INT0		PORTD, 0
#define HW_SDA		PORTD, 1
#define HW_INT1		PORTD, 1
#define HW_RXD1		PORTD, 2
#define HW_INT2		PORTD, 2
#define HW_TXD1		PORTD, 3
#define HW_INT3		PORTD, 3
#define HW_ICP1		PORTD, 4
#define HW_XCK1		PORTD, 5
#define HW_T1		PORTD, 6
#define HW_T2		PORTD, 7

/* Port E , Functional Names*/
#define HW_RXD0		PORTE, 0
#define HW_PDI		PORTE, 0
#define HW_TXD0		PORTE, 1
#define HW_PDO		PORTE, 1
#define HW_XCK0		PORTE, 2
#define HW_AIN0		PORTE, 2
#define HW_OC3A		PORTE, 3
#define HW_AIN1		PORTE, 3
#define HW_OC3B		PORTE, 4
#define HW_INT4		PORTE, 4
#define HW_OC3C		PORTE, 5
#define HW_INT5		PORTE, 5
#define HW_T3		PORTE, 6
#define HW_INT6		PORTE, 6
#define HW_ICP3		PORTE, 7
#define HW_INT7		PORTE, 7

/* Port F , Functional Names*/
#define HW_ADC0		PORTF, 0
#define HW_ADC1		PORTF, 1
#define HW_ADC2		PORTF, 2
#define HW_ADC3		PORTF, 3
#define HW_ADC4		PORTF, 4
#define HW_TCK		PORTF, 4
#define HW_ADC5		PORTF, 5
#define HW_TMS		PORTF, 5
#define HW_ADC6		PORTF, 6
#define HW_TDO		PORTF, 6
#define HW_ADC7		PORTF, 7
#define HW_TDI		PORTF, 7

/* Port G , Functional Names*/
#define HW_WR		PORTG, 0
#define HW_RD		PORTG, 1
#define HW_ALE		PORTG, 2
#define HW_TOSC2		PORTG, 3
#define HW_TOSC1		PORTG, 4


/*
** OSU ATMEGA128 DEFS 
*/

#define HW_LED1		PORTB, 0
#define HW_LED2		PORTB, 1
#define HW_LED3		PORTB, 2
#define HW_LED4		PORTB, 3
#define HW_LED5		PORTB, 4
#define HW_LED6		PORTB, 5
#define HW_LED7		PORTB, 6
#define HW_LED8		PORTB, 7

#define HW_SW1		PORTD, 0
#define HW_SW2		PORTD, 1
#define HW_SW3		PORTD, 2
#define HW_SW4		PORTD, 3
#define HW_SW5		PORTD, 4
#define HW_SW6		PORTD, 5
#define HW_SW7		PORTD, 6
#define HW_SW8		PORTD, 7

/******************************************************************************
 *                              T Y P E D E F S                               *
 ******************************************************************************/


/******************************************************************************
 *          P U B L I C   F U N C T I O N   D E C L A R A T I O N S           *
 ******************************************************************************/

#endif /* HW_PIN_DEFINITIONS_ */
