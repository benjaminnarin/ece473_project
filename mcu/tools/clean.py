#!/usr/bin/env python

import subprocess
import os

class cd:
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

with cd("../../../../"):
    #subprocess.call(["scons", "--whitelisted_components=BCCEN", "--warn=no-visual-c-missing", "--all_configs", "-c"], shell=True)

