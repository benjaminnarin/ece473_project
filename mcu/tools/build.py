#!/usr/bin/env python

import subprocess
import os
import sys


class cd:
    def __init__(self, newPath):
        self.newPath = newPath

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

with cd("../../../../"):
    if len(sys.argv) > 1:
        config_id = int(sys.argv[1])
    else:
        config_id = 2
    #scons_cmd = ["scons", "--whitelisted_components=BCCEN", "--config-id=" + str(config_id), "--no_unit_test", "-j8", "--quiet"]
    subprocess.call(scons_cmd, shell=True)

