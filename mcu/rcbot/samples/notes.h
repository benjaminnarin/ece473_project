

#define DEV_LED_PIN(name, port, bit) \
  INLINE void    DEV_LED##name##_set(void)      { PORT##port |= (1 << bit); } \
  INLINE void    DEV_LED##name##_clr(void)      { PORT##port &= ~(1 << bit); } \
  INLINE void    DEV_LED##name##_toggle(void)   { PORT##port ^= (1 << bit); } \
  INLINE void    DEV_LED##name##_in(void)       { DDR##port &= ~(1 << bit); PORT##port &= ~(1 << bit); } \
  INLINE void    DEV_LED##name##_out(void)      { DDR##port |= (1 << bit); } \
  INLINE void    DEV_LED##name##_pullup(void)   { PORT##port |= (1 << bit); } \
  INLINE uint8_t DEV_LED##name##_read(void)     { return (PIN##port & (1 << bit)) != 0; } \
  INLINE uint8_t DEV_LED##name##_state(void)    { return (DDR##port & (1 << bit)) != 0; }

// LED Definitions in halLED.h