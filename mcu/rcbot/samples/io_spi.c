/*
** SPI Functions and necessary includes
*/

#include "io_spi.h"
#include "rfr2.h"

int spi_init(void){
	volatile char IOReg;
	
	/* Set MOSI and SCK output, all others input */
	DDR_SPI = (1<<DD_MOSI)|(1<<DD_SCK)|(0<<DD_MISO);
	/* Enable  SPI, MSB First, Master, Clock Idle High, Send on Rising Edge, set clock rate fck/16 */
	SPCR = (1<<SPE)|(0<<DORD)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(1<<SPR0);
	
	IOReg = SPSR; //Clears SPSR and SPDR Register
	IOReg = SPDR;

	//Return 0 on Success
	return 0;
}

int spi_pinit(void){
	volatile char IOReg;
	
	/* Set MOSI and SCK output, all others input */
	DDR_SPI |= (1<<DD_MOSI)|(1<<DD_SCK);
	DDR_SPI &= ~(1<<DD_MISO);
	/* Enable  SPI, MSB First, Master, Clock Idle High, Send on Rising Edge, set clock rate fck/16 */
	SPCR = (1<<SPE)|(0<<DORD)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(1<<SPR0);
	
	IOReg = SPSR; //Clears SPSR and SPDR Register
	IOReg = SPDR;	

	//Return 0 on Success
	return 0;
}


char spi_send_pol(char byte){
	/* Sends one Byte */
	SPDR = byte;
	/* Wait until Char is sent */
	while(!(SPSR & (1<<SPIF)));
	
	//return received byte
	return SPDR;
}

