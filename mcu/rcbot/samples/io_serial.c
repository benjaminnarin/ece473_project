// Will contain all of the necessary serial functions

#include <avr/io.h>
//#include "Macro.h"
#include "io_serial.h"

/* Serial Setup Function */
//Replace MYUBRR with equation to auto calculate baud
void USART0_Init(unsigned int baud){
	unsigned int ubrr = MYUBRR;
	/* Set Baud Rate */
	UBRR0H = (unsigned char)(ubrr>>8);
	UBRR0L = (unsigned char)ubrr;
	/* Enable reciever and transmitter */
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);
	/* Set Frame Format: 8 Data, 2 Stop Bit */
	UCSR0C = (1<<USBS0)|(3<<UCSZ00);
}


/** This function needs to writes a string to the UART. It must check that the UART is ready for a new byte and 
  return a 1 if the string was not sent. 
  @param [in] str This is a pointer to the data to be sent.
  @return The function returns a 1 or error and 0 on successful completion.*/
unsigned char sendStringUART0(char* data)
{
	unsigned char byte;
	int i = 0;					// length counting variable
	int length = strlen(data);	// finds length of string

	/* If the flag is clear start sending bytes */
	if ((UCSR0A & (1 << UDRE0)) == (1 << UDRE0)){	

		/* Use send each character as a byte */
		for(i = 0; i < length; ++i){
			byte = data[i];	
			/* When its clear send bit */		
			while((UCSR0A & (1<< UDRE0)) != (1 << UDRE0));
			USART0_Transmit(byte);
		}

		/* Return 0 if string is sent */
		return 0;
	}
}

//USART 0 Transmit Function
void USART0_Transmit(unsigned char data){
	/* Wait for empty transmit buffer */
	while(!(UCSR0A&(1<<UDRE0)));
	/* Put Data into buffer, sends the data */
	UDR0 = data;
}

//USART0 Receive
unsigned char USART0_Recieve(void){
	/* Wait for data to be recieved */
	while(!(UCSR0A&(1<<RXC0)));
	/* Get and return the receieved data from the buffer*/
	return UDR0;
}

void USART0_Flush( void ){
	unsigned char dummy;
	while ( UCSR0A & (1<<RXC0) ) dummy = UDR0;
}

void itoASCII(int num, char* array){
	if (num > 10){
		//Do nothing Yet
	}
}

void printError(char* message){

	while(sendStringUART0("ERROR:"));
	while(sendStringUART0(message));
	while(sendStringUART0("\r\n"));
}