/* Serial Functionality */


#ifndef IO_SERIAL_H
#define IO_SERIAL_H

#define BAUD 9600
#define DATABITS 8
#define STOPBITS 2
#define PARITY	0
//0 = none

#define BAUD_CAL (F_CPU/(2*BAUD))-1
#define SAMPUBRR (F_CPU/16/BAUD-1)

#define MYUBRR 51U 

#define ENDSTRING (uint8_t)'\n'

void USART0_Init(unsigned int baud);
unsigned char sendStringUART0(char* data);
void USART0_Transmit(unsigned char data);
unsigned char USART0_Recieve(void);
void USART_Flush(void);
void itoASCII(int num, char* array);
void printError(char* message);

#endif
