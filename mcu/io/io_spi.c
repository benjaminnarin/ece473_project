#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>

void spi_init(void){
    DDRB |=  0x07;  //Turn on SS, MOSI, SCLK
    //mstr mode, sck=clk/2, cycle 1/2 phase, low polarity, MSB 1st, no interrupts 
    SPCR=(1<<SPE) | (1<<MSTR); //enable SPI, clk low initially, rising edge sample
    SPSR=(1<<SPI2X); //SPI at 2x speed (8 MHz)  
}//spi_init

uint8_t spi_read(void){
	SPDR = 0x00; //"dummy" write to SPDR
	while (bit_is_clear(SPSR,SPIF)){} //wait till 8 clock cycles are done
	return(SPDR); //return incoming data from SPDR
}//read_spi

void spi_write(uint8_t data){
	PORTF &= 0xFB; //port F bit 2, assert active low
	SPDR = data; //send data byte
	while (bit_is_clear(SPSR,SPIF)) {} //wait till data is sent out
	PORTF |= 0x04; //port F bit 2, deassert to logic high
} //spi_write