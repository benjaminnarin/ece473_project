#define	bit_set(...)		bit_set_(__VA_ARGS__)
#define	bit_set_(x,y)		x |= 1<<y			// set a bit
#define	bit_clear(...)		bit_clear_(__VA_ARGS__)
#define	bit_clear_(x,y)		x &= ~(1<<y)			// clear a bit
#define	bit_test(...)		bit_test_(__VA_ARGS__)
#define	bit_test_(x,y)		(!!(x & (1<<y)))		// test a bit

/*** following macros related to the PORTx - register only !	***/
/***								***/
#define	bit_dir_outp(...)	bit_dir_outp_(__VA_ARGS__)
#define	bit_dir_outp_(x,y)	*(&x-1) |= 1<<y			// access DDRx of PORTx !
#define	bit_dir_inp(...)	bit_dir_inp_(__VA_ARGS__)
#define	bit_dir_inp_(x,y)	*(&x-1) &= ~(1<<y)		// access DDRx of PORTx !
#define	bit_test_in(...)	bit_test_in_(__VA_ARGS__)
#define	bit_test_in_(x,y)	(!!(*(&x-2) & (1<<y)))		// access PINx of PORTx !


#define	LED0		PORTB, 0
#define	KEY0		PORTD, 4

#define F_CPU 1000000UL // 1 MHz

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>



int main( void )
{
  bit_dir_inp( KEY0 );		// input
  bit_set( KEY0 );		// pullup on
  bit_set( LED0 );		// LED off
  bit_dir_outp( LED0 );		// output

  for(;;){
    if( bit_test_in( KEY0 ))
      bit_clear( LED0 );
    else
      bit_set( LED0 );
_delay_ms(100);
  }
}